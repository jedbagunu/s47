const txt_first_name = document.querySelector("#txt-first-name")
const txt_last_name = document.querySelector("#txt-last-name")
const span_full_name1 = document.querySelector("#span-full-name1")
const span_full_name2 = document.querySelector("#span-full-name2")

txt_first_name.addEventListener("keyup", () => {
    span_full_name1.innerHTML = txt_first_name.value
})

txt_last_name.addEventListener("keyup", () => {
    span_full_name2.innerHTML = txt_last_name.value
})


txt_first_name.addEventListener('keyup', (event) =>{
  //event.target is the same as current element. works the same with code below
/*	console.log(txt_first_name)
	console.log(txt_first_name.value)*/

	console.log(event.target)
	console.log(event.target.value)
})
txt_last_name.addEventListener('keyup', (event) =>{
	console.log(event.target)
	console.log(event.target.value)
})

